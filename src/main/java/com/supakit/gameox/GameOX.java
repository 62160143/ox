
package com.supakit.gameox;

import java.util.Scanner;

public class GameOX {
     public static void main(String args[]) {
        Scanner kb = new Scanner(System.in);
        String[][] ox = new String[4][4];
        ox[0][0] = " ";
        for (Integer i = 1; i < ox.length; i++) {
            ox[0][i] = String.valueOf(i);
            ox[i][0] = String.valueOf(i);
        }
        for (int i = 1; i < 4; i++) {
            for (int j = 1; j < 4; j++) {
                ox[i][j] = "-";
            }
        }

        System.out.println("Choose the first starter!!!");

        char starter = kb.next().charAt(0);
        while (starter != 'x' && starter != 'X' && starter != 'o' && starter != 'O') {
            System.out.println("Choose the first starter try again!!!");
            starter = kb.next().charAt(0);
        }
        System.out.println("Welcome to OX Game");
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(ox[i][j] + " ");
            }
            System.out.println();
        }

        for (int i = 0; i < 9; i++) {
            if (i == 0) {
                System.out.println(Character.toUpperCase(starter) + " turn");
            } else if (i != 0 && i % 2 == 0) {
                if (starter == 'x' || starter == 'X') {
                    starter = 'O';
                    System.out.println(Character.toUpperCase(starter) + " turn");
                } else if (starter == 'o' || starter == 'O') {
                    starter = 'X';
                    System.out.println(Character.toUpperCase(starter) + " turn");
                }
            } else {
                if (starter == 'x' || starter == 'X') {
                    starter = 'O';
                    System.out.println(Character.toUpperCase(starter) + " turn");
                } else if (starter == 'o' || starter == 'O') {
                    starter = 'X';
                    System.out.println(Character.toUpperCase(starter) + " turn");
                }
            }

            System.out.println("Please input Row Col :");
            int count = 0;
            int row = kb.nextInt();
            int col = kb.nextInt();
            while ((row > 3 || col > 3) || (!ox[row][col].equals("-"))) {
                System.out.println("Please input Row Col try again!!! :");
                row = kb.nextInt();
                col = kb.nextInt();

            }
            ox[row][col] = String.valueOf(Character.toUpperCase(starter));
            for (int k = 0; k < 4; k++) {
                for (int l = 0; l < 4; l++) {
                    System.out.print(ox[k][l] + " ");
                }
                System.out.println();
            }
            if (ox[1][1].equals(ox[2][1]) && ox[2][1].equals(ox[3][1]) && !ox[1][1].equals("-")) {
                System.out.println("Player " + ox[1][1] + " win" + "\nBye bye ...");
                break;
            }
            if (ox[1][2].equals(ox[2][2]) && ox[2][2].equals(ox[3][2]) && !ox[1][2].equals("-")) {
                System.out.println("Player " + ox[1][2] + " win" + "\nBye bye ...");
                break;
            }
            if (ox[1][3].equals(ox[2][3]) && ox[2][3].equals(ox[3][3]) && !ox[1][3].equals("-")) {
                System.out.println("Player " + ox[1][1] + " win" + "\nBye bye ...");
                break;
            }

            if (ox[1][1].equals(ox[1][2]) && ox[1][2].equals(ox[1][3]) && !ox[1][1].equals("-")) {
                System.out.println("Player " + ox[1][1] + " win" + "\nBye bye ...");
                break;
            }
            if (ox[2][1].equals(ox[2][2]) && ox[2][2].equals(ox[2][3]) && !ox[2][1].equals("-")) {
                System.out.println("Player " + ox[2][1] + " win" + "\nBye bye ...");
                break;
            }
            if (ox[3][1].equals(ox[3][2]) && ox[3][2].equals(ox[3][3]) && !ox[3][1].equals("-")) {
                System.out.println("Player " + ox[3][1] + " win" + "\nBye bye ...");
                break;
            }

            if (ox[1][1].equals(ox[2][2]) && ox[2][2].equals(ox[3][3]) && !ox[1][1].equals("-")) {
                System.out.println("Player " + ox[1][1] + " win" + "\nBye bye ...");
                break;
            }
            if (ox[3][1].equals(ox[2][2]) && ox[2][2].equals(ox[1][3]) && !ox[3][1].equals("-")) {
                System.out.println("Player " + ox[3][1] + " win" + "\nBye bye ...");
                break;
            } else {
                for (int a = 1; a < 4; a++) {
                    for (int b = 1; b < 4; b++) {
                        if (ox[a][b].equals("-")) {
                            count++;
                        }
                    }
                }
                if (count == 0) {
                    System.out.println("Game over, tie!!! " + "\nBye bye ...");
                }
            }

        }

    }
    
    
    
}
